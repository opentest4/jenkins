from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
import requests

def get_jenkins_logs():
    # Replace with your Jenkins URL, job name, and build number
    url = "http://localhost:8080/job/hello_world_job/lastBuild/consoleText"
    response = requests.get(url, auth=('fouda', 'jenkinsjenkins'))  # Replace with your Jenkins username and password

    # Check the response status
    if response.status_code == 200:
        logs = response.text

        # Save the logs to a file
        with open('/opt/airflow/logs/logs.txt', 'w') as f:
            f.write(logs)
    else:
        print(f"Failed to get logs: {response.status_code}")

# Define the DAG
dag = DAG(
    'jenkins_logs',
    default_args={'owner': 'airflow'},
    start_date=days_ago(1),
)

# Define the task
get_logs_task = PythonOperator(
    task_id='get_jenkins_logs',
    python_callable=get_jenkins_logs,
    dag=dag,
)
