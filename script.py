import requests

jenkins_url = "http://localhost:8080"  # replace with your Jenkins server URL
job_name = "hello_world_job"  # replace with your job name
build_number = "1"  # replace with your build number
user = "admin"  # replace with your Jenkins user
token = "token"  # replace with your Jenkins API token

job_url = f"{jenkins_url}/job/{job_name}/{build_number}/consoleText"
response = requests.get(job_url, auth=(user, token))

if response.status_code == 200:
    with open('jenkins_job_log.txt', 'w') as file:
        file.write(response.text)
else:
    print(f"Failed to fetch logs with status code: {response.status_code}")
