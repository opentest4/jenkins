from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from datetime import datetime

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    # Removed start_date as it's not needed
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG(
    'my_dag',
    default_args=default_args,
    schedule_interval=None  # Removed schedule as you want manual triggering,
) as dag:

    run_this = KubernetesPodOperator(
        namespace='airflow',
        # Use a lighter image like alpine:latest for this simple task
        image="alpine:latest",
        cmds=["bash", "-cx"],
        arguments=["echo", "10"],
        labels={"foo": "bar"},
        name="airflow-test-pod",
        task_id="run_this",
        is_delete_operator_pod=True,
        hostnetwork=False,
    )
