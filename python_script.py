import requests
import json

# Replace with your details
JENKINS_URL = "https://4054-102-153-29-45.ngrok-free.app"
JENKINS_JOB_NAME = "hello_world_job"
user = "admin"
token = "119a4b501a8b99502075ea8d1ce2e36f8e"

def get_jenkins_logs(job_name):
    url = f"{JENKINS_URL}/job/{job_name}/lastBuild/consoleText"
    
    response = requests.get(url, auth=(user, token))
    if response.status_code == 200:
        return response.text
    else:
        raise Exception(f"Failed to retrieve logs: {response.status_code}")

def process_logs(logs):
    # Replace this with your desired filtering logic
    filtered_logs = [line for line in logs.splitlines() if "Hello World" in line]
    return filtered_logs

def save_logs_to_json(logs, filename):
    with open(filename, "w") as f:
        json.dump(logs, f, indent=4)

if __name__ == "__main__":
    try:
        logs = get_jenkins_logs(JENKINS_JOB_NAME)
        filtered_logs = process_logs(logs)
        save_logs_to_json(filtered_logs, "jenkins_logs.json")
        print("Logs successfully collected and saved to JSON file.")
    except Exception as e:
        print(f"An error occurred: {e}")
